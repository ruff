{-# LANGUAGE DeriveDataTypeable #-}
{- |
Module      :  Fractal.Mandelbrot.Symbolic.AngledInternalAddress
Copyright   :  (c) Claude Heiland-Allen 2010-2012
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  DeriveDataTypeable

Implementation of ideas from Dierk Schleicher's paper
/Internal Addresses Of The Mandelbrot Set And Galois Groups Of Polynomials (version of February 5, 2008)/
<http://arxiv.org/abs/math/9411238v2>.

-}
module Fractal.Mandelbrot.Symbolic.AngledInternalAddress
  ( AngledInternalAddress(Unangled, Angled)
  , angledInternalAddress
  , angledFromList
  , angledToList
  , externalAngles
  , stripAngles
  , splitAddress
  , joinAddress
  , addressPeriod
  , visibleComponents
  ) where

import Data.Data (Data())
import Data.Typeable (Typeable())

import Data.List (genericDrop, genericIndex, genericLength, genericReplicate)
import Data.Ratio ((%))

import Fractal.Mandelbrot.Symbolic.ExternalAngle (anglePeriod, doubleAngle, ExternalAngle(ExternalAngle), ExternalAnglePair(ExternalAnglePair), BinaryAnglePair(), toBinaryAnglePair, fromBinaryAnglePair, Period(Period), tuneBinaryPair, wrapAngle)
import Fractal.Mandelbrot.Symbolic.InternalAngle (InternalAngle(InternalAngle), (*/), FareyAngle(FareyAngle), fareyAngle)
import Fractal.Mandelbrot.Symbolic.KneadingSequence (Kneading, kneading, kneadingPeriod, unwrapKneading)
import Fractal.Mandelbrot.Symbolic.InternalAddress (InternalAddress(InternalAddress), internalAddress, internalToList)
import Fractal.Mandelbrot.Utils (chunkWith2, mod_, strictlyWithin)


rho :: Kneading -> Integer -> Integer
rho v r | r >= 1 && fmap (Period r`mod`) (kneadingPeriod v) /= Just 0 = ((1 + r) +) . genericLength . takeWhile id . zipWith (==) vs . genericDrop r $ vs
        | otherwise = rho v (r + 1)
  where
    vs = unwrapKneading v

orbit :: (a -> a) -> a -> [a]
orbit = iterate

-- | Angled internal addresses have angles between each integer in an
--   internal address.
data AngledInternalAddress
  = Unangled Period
  | Angled Period InternalAngle AngledInternalAddress
  deriving (Read, Show, Eq, Ord, Data, Typeable)

-- | Builds a valid 'AngledInternalAddress' from a list, checking the
--   precondition that only the last 'Maybe Angle' should be 'Nothing',
--   and the 'Integer' must be strictly increasing.
angledFromList :: [(Period, Maybe InternalAngle)] -> Maybe AngledInternalAddress
angledFromList = fromList' 0
  where
    fromList' x [(n, Nothing)] | n > x = Just (Unangled n)
    fromList' x ((n, Just r) : xs) | n > x && 0 < r && r < 1 = Angled n r `fmap` fromList' n xs
    fromList' _ _ = Nothing

unsafeAngledFromList :: [(Period, Maybe InternalAngle)] -> AngledInternalAddress
unsafeAngledFromList = fromList' 0
  where
    fromList' x [(n, Nothing)] | n > x = Unangled n
    fromList' x ((n, Just r) : xs) | n > x && 0 < r && r < 1 = Angled n r (fromList' n xs)
    fromList' _ _ = error "Fractal.Mandelbrot.Address.unsafeAngledFromList"

-- | Convert an 'AngledInternalAddress' to a list.
angledToList :: AngledInternalAddress -> [(Period, Maybe InternalAngle)]
angledToList (Unangled n) = [(n, Nothing)]
angledToList (Angled n r a) = (n, Just r) : angledToList a

denominators :: InternalAddress -> Kneading -> [Integer]
denominators a v = denominators' (internalToList a)
  where
    denominators' (Period s0:ss@(Period s1:_)) =
      let rr = s1 `mod_` s0
      in  (((s1 - rr) `div` s0) + if s0 `elem` takeWhile (<= s0) (orbit p rr) then 1 else 2) : denominators' ss
    denominators' _ = []
    p = rho v

numerators :: ExternalAngle -> InternalAddress -> [Integer] -> [Integer]
numerators r a qs = zipWith num (internalToList a) qs
  where
    num s q = genericLength . filter (<= r) . map (genericIndex rs) $ [0 .. q - 2]
      where
        rs = iterate (foldr (.) id . genericReplicate s $ doubleAngle) r

-- | The angled internal address corresponding to an external angle.
angledInternalAddress :: ExternalAngle -> Maybe AngledInternalAddress
angledInternalAddress r0 = do
  let r = wrapAngle r0
      k = kneading r
  i <- internalAddress k
  let d = denominators i k
      n = numerators r i d
  return . unsafeAngledFromList . zip (internalToList i) . (++ [Nothing]) . map (Just . InternalAngle) . zipWith (%) n $ d

-- | Split an angled internal address at the last island.
splitAddress :: AngledInternalAddress -> (AngledInternalAddress, [InternalAngle])
splitAddress a =
  let (ps0, rs0) = unzip $ angledToList a
      ps1 = reverse ps0
      rs1 = reverse (Nothing : init rs0)
      prs1 = zip ps1 rs1
      f ((p, Just r):qrs@((q, _):_)) acc
        | p == q */ r = f qrs (r : acc)
      f prs acc = g prs acc
      g prs acc =
        let (ps2, rs2) = unzip prs
            ps3 = reverse ps2
            rs3 = reverse (Nothing : init rs2)
            prs3 = zip ps3 rs3
            aa = unsafeAngledFromList prs3
        in  (aa, acc)
  in  f prs1 []

-- | The inverse of 'splitAddress'.
joinAddress :: AngledInternalAddress -> [InternalAngle] -> AngledInternalAddress
joinAddress (Unangled p) [] = Unangled p
joinAddress (Unangled p) (r:rs) = Angled p r (joinAddress (Unangled $ p */ r) rs)
joinAddress (Angled p r a) rs = Angled p r (joinAddress a rs)

-- | The period of an angled internal address.
addressPeriod :: AngledInternalAddress -> Period
addressPeriod (Unangled p) = p
addressPeriod (Angled _ _ a) = addressPeriod a

-- | Discard angle information from an internal address.
stripAngles :: AngledInternalAddress -> InternalAddress
stripAngles = InternalAddress . map fst . angledToList

-- | The pair of external angles whose rays land at the root of the
--   hyperbolic component described by the angled internal address.
externalAngles :: AngledInternalAddress -> Maybe ExternalAnglePair
externalAngles = externalAngles' 1 (toBinaryAnglePair (ExternalAnglePair 0 1))

externalAngles' :: Period -> BinaryAnglePair -> AngledInternalAddress -> Maybe ExternalAnglePair
externalAngles' p0 lohi (Unangled p)
  | p0 /= p = case visibleComponents (fromBinaryAnglePair lohi) p of
      [lh] -> Just lh
      _ -> Nothing
  | otherwise = Just (fromBinaryAnglePair lohi)
externalAngles' p0 lohi a0@(Angled p r a)
  | p0 /= p = case visibleComponents (fromBinaryAnglePair lohi) p of
      [lh] -> externalAngles' p (toBinaryAnglePair lh) a0
      _ -> Nothing
  | otherwise = do
      let FareyAngle _ lh = fareyAngle r
      externalAngles' (p */ r) (if p > 1 then tuneBinaryPair lh lohi else lh) a

-- | The visible components in the wake.
visibleComponents :: ExternalAnglePair -> Period -> [ExternalAnglePair]
visibleComponents (ExternalAnglePair lo hi) q =
  let gaps (l, h) n
        | n == 0 = [(l, h)]
        | n > 0 = let gs = gaps (l, h) (n - 1)
                      cs = candidates n gs
                  in  accumulate cs gs
        -- deliberately unhandled case
      candidates n gs =
        let den = 2 ^ n - 1
        in  [ r
            | (l, h) <- gs
            , num <- [ ceiling (l * fromInteger den)
                      .. floor (h * fromInteger den) ]
            , let r = ExternalAngle $ num % den
            , l < r, r < h
            , anglePeriod r == Just n
            ]
      accumulate [] ws = ws
      accumulate (l : h : lhs) ws =
        let (ls, ms@((ml, _):_)) = break (l `strictlyWithin`) ws
            (_s, (_, rh):rs) = break (h `strictlyWithin`) ms
        in  ls ++ [(ml, l)] ++ accumulate lhs ((h, rh) : rs)
      -- deliberately unhandled case
  in  chunkWith2 ExternalAnglePair . candidates q . gaps (lo, hi) $ (q - 1)
