{-# LANGUAGE DeriveDataTypeable #-}
{- |
Module      :  Fractal.Mandelbrot.Symbolic.InternalAddress
Copyright   :  (c) Claude Heiland-Allen 2010-2012
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  DeriveDataTypeable

Implementation of ideas from Dierk Schleicher's paper
/Internal Addresses Of The Mandelbrot Set And Galois Groups Of Polynomials (version of February 5, 2008)/
<http://arxiv.org/abs/math/9411238v2>.

-}

module Fractal.Mandelbrot.Symbolic.InternalAddress
  ( InternalAddress(InternalAddress)
  , internalAddress
  , internalFromList
  , internalToList
  , associatedKneadings
  , upperKneading
  , lowerKneading
  ) where

import Data.Data (Data())
import Data.Typeable (Typeable())
import Data.List (genericLength, genericTake)

import Fractal.Mandelbrot.Symbolic.ExternalAngle (Period)
import Fractal.Mandelbrot.Symbolic.KneadingSequence (Knead(..), Kneading(Aperiodic, Periodic, StarPeriodic), unwrapKneading)
import Fractal.Mandelbrot.Utils (divisors)

-- | Internal addresses are a non-empty sequence of strictly increasing
--   periods beginning with '1'.
newtype InternalAddress = InternalAddress [Period]
  deriving (Read, Show, Eq, Ord, Data, Typeable)

-- | Construct a valid 'InternalAddress', checking the precondition.
internalFromList :: [Period] -> Maybe InternalAddress
internalFromList x0s@(1:_) = InternalAddress `fmap` fromList' 0 x0s
  where
    fromList' n [x]    | x > n = Just [x]
    fromList' n (x:xs) | x > n = (x:) `fmap` fromList' x xs
    fromList' _ _ = Nothing
internalFromList _ = Nothing

-- | Extract the sequence of periods.
internalToList :: InternalAddress -> [Period]
internalToList (InternalAddress xs) = xs

-- | Construct an 'InternalAddress' from a kneading sequence.
internalAddress :: Kneading -> Maybe InternalAddress
internalAddress (StarPeriodic [Star])      = Just (InternalAddress [1])
internalAddress (StarPeriodic v@(One:_))   = Just . InternalAddress . address'per (genericLength v) $ v
internalAddress (Periodic     v@(One:_))   = Just . InternalAddress . address'per (genericLength v) $ v
internalAddress k@(Aperiodic    (One:_))   = Just . InternalAddress . address'inf . unwrapKneading $ k
internalAddress _ = Nothing

address'inf :: [Knead] -> [Period]
address'inf v = address' v

address'per :: Period -> [Knead] -> [Period]
address'per p v = takeWhile (<= p) $ address' v

address' :: [Knead] -> [Period]
address' v = address'' 1 [One]
  where
    address'' sk vk = sk : address'' sk' vk'
      where
        sk' = (1 +) . genericLength . takeWhile id . zipWith (==) v . cycle $ vk
        vk' = genericTake sk' (cycle v)

-- | A star-periodic kneading sequence's upper and lower associated
--   kneading sequences.
associatedKneadings :: Kneading -> Maybe (Kneading, Kneading)
associatedKneadings (StarPeriodic k) = Just (Periodic a, Periodic abar)
  where
    n = genericLength k
    abar:_ = filter (and . zipWith (==) a' . cycle) . map (`genericTake` a') . divisors $ n
    (a, a') = if ((n `elem`) . internalToList) `fmap` internalAddress (Periodic a1) == Just True then (a1, a2) else (a2, a1)
    a1 = map (\s -> case s of Star -> Zero ; t -> t) k
    a2 = map (\s -> case s of Star -> One  ; t -> t) k
associatedKneadings _ = Nothing

-- | The upper associated kneading sequence.
upperKneading :: Kneading -> Maybe Kneading
upperKneading = fmap fst . associatedKneadings

-- | The lower associated kneading sequence.
lowerKneading :: Kneading -> Maybe Kneading
lowerKneading = fmap snd . associatedKneadings
