{-# LANGUAGE DeriveDataTypeable, GeneralizedNewtypeDeriving #-}
{- |
Module      :  Fractal.Mandelbrot.ExternalAngle
Copyright   :  (c) Claude Heiland-Allen 2010-2012
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  DeriveDataTypeable, GeneralizedNewtypeDeriving

External angles and tuning transformations.

Described with examples in (and in many others) G Pastor et al's paper
/Operating with external arguments in the Mandelbrot set antenna/
<http://www.iec.csic.es/~fausto/publica/Pastor02_pre.pdf>.

-}
module Fractal.Mandelbrot.Symbolic.ExternalAngle
  ( ExternalAngle(ExternalAngle)
  , wrapAngle
  , doubleAngle
  , doublingPreimages
  , ExternalAnglePair(ExternalAnglePair)
  , externalAnglePair
  , BinaryAngle(BinaryAngle)
  , fromBinaryAngle
  , toBinaryAngle
  , BinaryAnglePair(BinaryAnglePair)
  , fromBinaryAnglePair
  , toBinaryAnglePair
  , tuneBinary
  , tuneBinaryPair
  , tune
  , tunePair
  , Period(Period)
  , anglePeriod
  , Preperiod(Preperiod)
  , anglePreperiod
  ) where

import Data.Data (Data())
import Data.Typeable (Typeable())
import Data.Bits (testBit)
import Data.List (genericLength)
import Data.Ratio ((%), numerator, denominator)

import Fractal.Mandelbrot.Utils (fromBits, genericElemIndex)

-- | External angles define external rays, which land on the boundary.
--   In particular, each hyperbolic component has two rays that land at
--   its root (by convention the unique hyperbolic of period 1 has
--   external angles 0 and 1).
newtype ExternalAngle = ExternalAngle Rational
  deriving (Eq, Ord, Read, Show, Data, Typeable, Enum, Num, Fractional, Real, RealFrac)

-- | Wrap an external angle into [0, 1).
wrapAngle :: ExternalAngle -> ExternalAngle
wrapAngle a
  | f < 0 = 1 + f
  | otherwise = f
  where
    (_, f) = properFraction a :: (Integer, ExternalAngle)

-- | External angle doubling map.
doubleAngle :: ExternalAngle -> ExternalAngle
doubleAngle a = wrapAngle (2 * a)

-- | Pre-images under angle doubling.
doublingPreimages :: ExternalAngle -> (ExternalAngle, ExternalAngle)
doublingPreimages e0 = let e = wrapAngle e0 in (e / 2, (e + 1) / 2)

-- | A pair of external angles whose rays land at the root of the same
--   hyperbolic component, moreover @'ExternalAnglePair' lo hi@ also
--   satisfies @lo < hi@.
data ExternalAnglePair = ExternalAnglePair ExternalAngle ExternalAngle
  deriving (Eq, Ord, Read, Show, Data, Typeable)

-- | Put a pair of external angles that land at the root of the same
--   hyperbolic component into proper order.  The precondition is not
--   checked.
externalAnglePair :: ExternalAngle -> ExternalAngle -> ExternalAnglePair
externalAnglePair x y | x < y  = ExternalAnglePair x y
                      | x > y  = ExternalAnglePair y x
                      | x == 0 = ExternalAnglePair 0 1
                      -- deliberately unhandled case

-- | Binary representation of a (pre-)periodic external angle.
--   Big endian lists of bits for pre-period and period.
data BinaryAngle = BinaryAngle [Bool] [Bool]
  deriving (Eq, Ord, Read, Show, Data, Typeable)

-- | A pair of binary angles whose rays land at the root of the same
--   hyperbolic component, with ordering constraints similar to
--   'ExternalAnglePair'.
data BinaryAnglePair = BinaryAnglePair BinaryAngle BinaryAngle
  deriving (Eq, Ord, Read, Show, Data, Typeable)

-- | Convert an angle from binary representation.
fromBinaryAngle :: BinaryAngle -> ExternalAngle
fromBinaryAngle (BinaryAngle pre per)
  | n == 0    = ExternalAngle $  fromBits pre % (2 ^ m)
  | otherwise = ExternalAngle $ (fromBits pre % (2 ^ m)) + (fromBits per % (2 ^ m * (2 ^ n - 1)))
  where
    m, n :: Integer
    m = genericLength pre
    n = genericLength per

-- | Convert an angle to binary representation.
toBinaryAngle :: ExternalAngle -> BinaryAngle
toBinaryAngle e@(ExternalAngle a)
  | a == 0 = BinaryAngle [] []
  | even (denominator a) =
      let BinaryAngle pre per = toBinaryAngle (doubleAngle e)
          b = a >= 1/2
      in  BinaryAngle (b:pre) per
  | otherwise =
      let (t, p):_ = dropWhile ((1 /=) . denominator . fst) . map (\q -> (a * (2^q - 1), q)) $ [ 1 ..]
          s = numerator t
          per = [ s `testBit` i | i <- [p - 1, p - 2 .. 0] ]
      in  BinaryAngle [] per

-- | Convert an angle pair from binary representation.
fromBinaryAnglePair :: BinaryAnglePair -> ExternalAnglePair
fromBinaryAnglePair (BinaryAnglePair x y) = ExternalAnglePair (fromBinaryAngle x) (fromBinaryAngle y)

-- | Convert an angle pair to binary representation.
toBinaryAnglePair :: ExternalAnglePair -> BinaryAnglePair
toBinaryAnglePair (ExternalAnglePair x y) = BinaryAnglePair (toBinaryAngle x) (toBinaryAngle y)

-- | Tuning transformation for binary represented periodic angles.
--   Probably only valid for angle pairs presenting ray pairs.
tuneBinary :: BinaryAngle -> BinaryAnglePair -> BinaryAngle
tuneBinary (BinaryAngle tpre tper) (BinaryAnglePair (BinaryAngle [] per0) (BinaryAngle [] per1))
  = BinaryAngle (concatMap f tpre) (concatMap f tper)
  where
    f False = per0
    f True  = per1
-- tuneBinary unhandled case, FIXME

-- | Tuning transformation lifted to binary pairs.
tuneBinaryPair :: BinaryAnglePair -> BinaryAnglePair -> BinaryAnglePair
tuneBinaryPair (BinaryAnglePair x y) t0t1 = BinaryAnglePair (tuneBinary x t0t1) (tuneBinary y t0t1)

-- | Tuning transformation for external angles.  The implementation
--   transits via binary angles.
tune :: ExternalAngle -> ExternalAnglePair -> ExternalAngle
tune t t0t1 = fromBinaryAngle $ tuneBinary (toBinaryAngle t) (toBinaryAnglePair t0t1)

-- | Tuning transformation lifted to pairs.  The implementation
--   transits via binary angle pairs.
tunePair :: ExternalAnglePair -> ExternalAnglePair -> ExternalAnglePair
tunePair xy t0t1 = fromBinaryAnglePair (tuneBinaryPair (toBinaryAnglePair xy) (toBinaryAnglePair t0t1))

-- | Periods.
newtype Period = Period Integer
  deriving (Eq, Ord, Read, Show, Data, Typeable, Enum, Num, Integral, Real)

-- | The period of an external angle, or 'Nothing' when it is pre-periodic.
anglePeriod :: ExternalAngle -> Maybe Period
anglePeriod e@(ExternalAngle r)
  | even (denominator r) = Nothing
  | otherwise = (1 +) `fmap` (genericElemIndex e . drop 1 . iterate doubleAngle) e

-- | Pre-periods.
newtype Preperiod = Preperiod Integer
  deriving (Eq, Ord, Read, Show, Data, Typeable, Enum, Num, Integral, Real)

-- | The pre-period of an external angle, 0 when it is strictly periodic.
anglePreperiod :: ExternalAngle -> Preperiod
anglePreperiod e = let BinaryAngle pre _ = toBinaryAngle e in genericLength pre
