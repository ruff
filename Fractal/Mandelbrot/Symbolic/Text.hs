{- |
Module      :  Fractal.Mandelbrot.Symbolic.Text
Copyright   :  (c) Claude Heiland-Allen 2010-2012
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  portable

Human readable formatting.

-}
module Fractal.Mandelbrot.Symbolic.Text
  ( prettyAddress
  , parseAddress
  , prettyConciseAddress
  , parseConciseAddress
  , prettyConciseItem
  , parseConciseItem
  , prettyInternalAngle
  , parseInternalAngle
  , prettyExternalAngle
  , parseExternalAngle
  , prettyPeriod
  , parsePeriod
  , prettyInteger
  , parseInteger
  , prettyRational
  , parseRational
  ) where

import Control.Monad (liftM2)
import Data.Ratio ((%), numerator, denominator)

import Fractal.Mandelbrot.Symbolic.ExternalAngle (ExternalAngle(ExternalAngle), Period(Period))
import Fractal.Mandelbrot.Symbolic.InternalAngle (InternalAngle(InternalAngle))
import Fractal.Mandelbrot.Symbolic.AngledInternalAddress (AngledInternalAddress())
import Fractal.Mandelbrot.Symbolic.ConciseAddress(ConciseAddress(ConciseAddress), ConciseItem(ConciseAngle, ConcisePeriod), toConciseAddress, fromConciseAddress)

prettyInteger :: Integer -> String
prettyInteger i = show i

prettyRational :: Rational -> String
prettyRational r = show (numerator r) ++ "/" ++ show (denominator r)

prettyPeriod :: Period -> String
prettyPeriod (Period p) = prettyInteger p

prettyInternalAngle :: InternalAngle -> String
prettyInternalAngle (InternalAngle r) = prettyRational r

prettyExternalAngle :: ExternalAngle -> String
prettyExternalAngle (ExternalAngle r) = prettyRational r

prettyConciseItem :: ConciseItem -> String
prettyConciseItem (ConcisePeriod p) = prettyPeriod p
prettyConciseItem (ConciseAngle  r) = prettyInternalAngle r

prettyConciseAddress :: ConciseAddress -> String
prettyConciseAddress (ConciseAddress a) = unwords (map prettyConciseItem a)

prettyAddress :: AngledInternalAddress -> String
prettyAddress = prettyConciseAddress . toConciseAddress

parseAddress :: String -> Maybe AngledInternalAddress
parseAddress = fmap fromConciseAddress . parseConciseAddress

parseConciseAddress :: String -> Maybe ConciseAddress
parseConciseAddress s = ConciseAddress `fmap` mapM parseConciseItem (words s)

parseConciseItem :: String -> Maybe ConciseItem
parseConciseItem s
 | '/' `elem` s = ConciseAngle  `fmap` parseInternalAngle s
 | otherwise    = ConcisePeriod `fmap` parsePeriod s

parseInternalAngle :: String -> Maybe InternalAngle
parseInternalAngle s = InternalAngle `fmap` parseRational s

parseExternalAngle :: String -> Maybe ExternalAngle
parseExternalAngle s = ExternalAngle `fmap` parseRational s

parsePeriod :: String -> Maybe Period
parsePeriod s = Period `fmap` parseInteger s

parseInteger :: String -> Maybe Integer
parseInteger s = case reads s of
  [(i,"")] -> Just i
  _ -> Nothing

parseRational :: String -> Maybe Rational
parseRational s = case break ('/' ==) s of
  (n,'/':d) -> liftM2 (\n' d' ->  (n' % d')) (parseInteger n) (parseInteger d)
  _ -> Nothing
