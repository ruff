{-# LANGUAGE DeriveDataTypeable, GeneralizedNewtypeDeriving #-}
{- |
Module      :  Fractal.Mandelbrot.Symbolic.Antenna
Copyright   :  (c) Claude Heiland-Allen 2010-2012
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  DeriveDataTypeable, GeneralizedNewtypeDeriving

An implementation of the algorithms described in
R L Devaney and M Moreno-Rocha's paper of April 11, 2000
/Geometry of the Antennas in the Mandelbrot Set/
<http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.28.1348&rep=rep1&type=pdf>.

-}
module Fractal.Mandelbrot.Symbolic.Antenna
  ( bulb
  , antenna
  , spokes
  ) where

import Data.List (genericTake, genericDrop)
import Data.Ratio (numerator, denominator)

import Fractal.Mandelbrot.Symbolic.ExternalAngle
import Fractal.Mandelbrot.Symbolic.InternalAngle

bulb' :: InternalAngle -> BinaryAnglePair
bulb' (InternalAngle pq) = BinaryAnglePair (BinaryAngle [] ls) (BinaryAngle [] us)
  where
    q = denominator (w pq)
    ls = is ++ [False, True]
    us = is ++ [True, False]
    is = genericTake (q - 2) . map i . iterate r $ pq
    i a = not $ 0 < a && a < 1 - pq
    r a = w (a + pq)
    w a
      | f < 0 = 1 + f
      | otherwise = f
      where
        (_, f) = properFraction a :: (Integer, Rational)

-- | Theorem 3.1: angles of the bulb.
bulb :: InternalAngle -> ExternalAnglePair
bulb = fromBinaryAnglePair . bulb'

antenna' :: BinaryAnglePair -> BinaryAnglePair
antenna' (BinaryAnglePair (BinaryAngle [] ls) (BinaryAngle [] us)) = (BinaryAnglePair (BinaryAngle ls us) (BinaryAngle us ls))

-- | Proposition 3.2: angles of the antenna.
antenna :: InternalAngle -> ExternalAnglePair
antenna = fromBinaryAnglePair . antenna' . bulb'

spokes' :: FareyAngle -> FareyAngle -> [BinaryAngle]
spokes' (FareyAngle (InternalAngle ab) _) (FareyAngle (InternalAngle pq) (BinaryAnglePair (BinaryAngle [] ls) (BinaryAngle [] us))) =
  [ BinaryAngle ls (rotate (k * b) us) | k <- [ 0 .. q - p - 1 ] ] ++
  [ BinaryAngle us (rotate (k * b) us) | k <- [ q - p .. q - 1 ] ]
  where
    p = numerator pq
    q = denominator pq
    b = denominator ab
    rotate i = genericTake q . genericDrop i . cycle

-- | Theorem 5.3: angles of the spokes
spokes :: InternalAngle -> [ExternalAngle]
spokes pq = map fromBinaryAngle (spokes' fab fpq)
  where
    (fab, fcd) = fareyParents pq
    fpq = fab <+> fcd
