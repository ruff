{-# LANGUAGE DeriveDataTypeable, GeneralizedNewtypeDeriving #-}
{- |
Module      :  Fractal.Mandelbrot.KneadingSequence
Copyright   :  (c) Claude Heiland-Allen 2010-2012
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  DeriveDataTypeable, GeneralizedNewtypeDeriving

Implementation of ideas from Dierk Schleicher's paper
/Internal Addresses Of The Mandelbrot Set And Galois Groups Of Polynomials (version of February 5, 2008)/
<http://arxiv.org/abs/math/9411238v2>.

-}
module Fractal.Mandelbrot.Symbolic.KneadingSequence
  ( Knead(..)
  , Kneading(..)
  , kneading
  , kneadingPeriod
  , unwrapKneading
  ) where

import Data.Data (Data())
import Data.Typeable (Typeable())
import Data.List (genericLength, genericSplitAt, genericTake)
import Data.Maybe (isJust, listToMaybe)

import Fractal.Mandelbrot.Symbolic.ExternalAngle (doubleAngle, doublingPreimages, ExternalAngle, Period(Period), wrapAngle)
import Fractal.Mandelbrot.Utils (strictlyWithin, strictlyWithout)

-- | Elements of kneading sequences.
data Knead
  = Zero
  | One
  | Star
  deriving (Read, Show, Eq, Ord, Enum, Bounded, Data, Typeable)

-- | Kneading sequences.  Note that the 'Aperiodic' case has an infinite list.
data Kneading
  = Aperiodic [Knead]
  | PrePeriodic [Knead] [Knead]
  | StarPeriodic [Knead]
  | Periodic  [Knead]
  deriving (Read, Show, Eq, Ord, Data, Typeable)

-- | The kneading sequence for an external angle.
kneading :: ExternalAngle -> Kneading
kneading a0'
  | a0 == 0 = StarPeriodic [Star]
  | otherwise = fst kneads
  where
    a0 = wrapAngle a0'
    lh = doublingPreimages a0
    kneads = kneading' 1 (doubleAngle a0)
    ks = (a0, One) : snd kneads
    kneading' :: Integer -> ExternalAngle -> (Kneading, [(ExternalAngle, Knead)])
    kneading' n a
      | isJust i = case i of
          Just 0 -> case last qs of
            Star -> (StarPeriodic qs, [])
            _    -> (Periodic qs, [])
          Just j -> let (p, q) = genericSplitAt j qs
                    in (PrePeriodic p q, [])
          -- unreachable
      | a `strictlyWithin`  lh = ((a, One ):) `mapP` k
      | a `strictlyWithout` lh = ((a, Zero):) `mapP` k
      | otherwise              = ((a, Star):) `mapP` k
      where
        k = kneading' (n+1) (doubleAngle a)
        ps = genericTake n ks
        qs = map snd ps
        i = fmap fst . listToMaybe . filter ((a ==) . fst . snd) . zip [(0 :: Integer) ..] $ ps
        mapP f ~(x, y) = (x, f y)

-- | The period of a kneading sequence, or 'Nothing' when it isn't periodic.
kneadingPeriod :: Kneading -> Maybe Period
kneadingPeriod (StarPeriodic k) = Just (Period $ genericLength k)
kneadingPeriod (Periodic k) = Just (Period $ genericLength k)
kneadingPeriod _ = Nothing

-- | Unwrap a kneading sequence to an infinite list.
unwrapKneading :: Kneading -> [Knead]
unwrapKneading (Aperiodic vs) = vs
unwrapKneading (PrePeriodic us vs) = us ++ cycle vs
unwrapKneading (StarPeriodic vs) = cycle vs
unwrapKneading (Periodic vs) = cycle vs
