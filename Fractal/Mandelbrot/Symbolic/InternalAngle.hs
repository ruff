{-# LANGUAGE DeriveDataTypeable, GeneralizedNewtypeDeriving #-}
{- |
Module      :  Fractal.Mandelbrot.Symbolic.InternalAngle
Copyright   :  (c) Claude Heiland-Allen 2010-2012
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  DeriveDataTypeable, GeneralizedNewtypeDeriving

A background to the ideas is presented in Robert L Devaney's paper
/The Mandelbrot Set and The Farey Tree/
<http://math.bu.edu/people/bob/papers/farey.ps>,
although the particular binary search algorithm used here was a
suggestion of Robert P Munafo.

-}
module Fractal.Mandelbrot.Symbolic.InternalAngle
  ( InternalAngle(InternalAngle)
  , (*/)
  , FareyAngle(FareyAngle)
  , fareyAngle
  , fareyParents
  , (<+>)
  , (</>)
  , fareyZero
  , fareyOne
  , fareyHalf
  ) where

import Data.Data (Data)
import Data.Typeable (Typeable)

import Data.Ratio ((%), numerator, denominator)

import Fractal.Mandelbrot.Symbolic.ExternalAngle (BinaryAngle(BinaryAngle), BinaryAnglePair(BinaryAnglePair), Period(Period))

-- | Internal angles label the neighbouring descendents of hyperbolic
--   components.  The descendent of a hyperbolic component with period
--   @p@ at internal angle @a@ has period @p * denominator a@
newtype InternalAngle = InternalAngle Rational
  deriving (Eq, Ord, Read, Show, Data, Typeable, Enum, Num, Fractional, Real, RealFrac)

-- | Multiply a period by the denominator of an internal angle.
(*/) :: Period -> InternalAngle -> Period
Period p */ InternalAngle a = Period (p * denominator a)
infixl 7 */

-- | Farey angles pair an internal angle together with the external
--   angles of the corresponding child of the top level cardioid.
data FareyAngle = FareyAngle InternalAngle BinaryAnglePair
  deriving (Eq, Ord, Read, Show, Data, Typeable)

-- | Special Farey angles.
fareyZero, fareyOne, fareyHalf :: FareyAngle
fareyZero = FareyAngle   0   (BinaryAnglePair (BinaryAngle [] [False]       ) (BinaryAngle [] [False]       ))
fareyOne  = FareyAngle   1   (BinaryAnglePair (BinaryAngle [] [True ]       ) (BinaryAngle [] [True ]       ))
fareyHalf = FareyAngle (1/2) (BinaryAnglePair (BinaryAngle [] [False, True ]) (BinaryAngle [] [True , False]))

-- | Farey addition of neighbours finding inner neighbour.
--   Precondition: a < b && a `neighbours` b && (a, b) /= (0, 1)
fareyAddition :: FareyAngle -> FareyAngle -> FareyAngle
fareyAddition
    (FareyAngle (InternalAngle a) (BinaryAnglePair (BinaryAngle [] al) (BinaryAngle [] ah)))
    (FareyAngle (InternalAngle b) (BinaryAnglePair (BinaryAngle [] bl) (BinaryAngle [] bh))) =
     FareyAngle (InternalAngle c) (BinaryAnglePair (BinaryAngle [] cl) (BinaryAngle [] ch))
  where
    c = (numerator a + numerator b) % (denominator a + denominator b)
    cl = p ++ l
    ch = p ++ h
    (p, l, h)
      | denominator a < denominator b = (ah, bl, bh)
      | denominator a > denominator b = (bl, al, ah)
      -- deliberately unhandled
-- deliberately unhandled

-- | Farey addition of neighbours finding inner neighbour.
--   Precondition: a `neighbours` b
(<+>) :: FareyAngle -> FareyAngle -> FareyAngle
-- special treatment for special cases
FareyAngle 0 _ <+> FareyAngle 0 _ = fareyZero
FareyAngle 1 _ <+> FareyAngle 1 _ = fareyOne
FareyAngle 0 _ <+> FareyAngle 1 _ = fareyHalf
FareyAngle 1 _ <+> FareyAngle 0 _ = fareyHalf
x@(FareyAngle a _) <+> y@(FareyAngle b _)
  | a < b = fareyAddition x y
  | b < a = fareyAddition y x
  -- deliberately unhandeled
infixl 6 <+>

-- | Farey search for parents.
--   Precondition: l < t && t < r && l `neighbours` r
fareySearch' :: FareyAngle -> FareyAngle -> InternalAngle -> (FareyAngle, FareyAngle)
fareySearch' l r t
  | t <  s = fareySearch' l m t
  | t == s = (l, r)
  | t >  s = fareySearch' m r t
  where m@(FareyAngle s _) = l <+> r

-- | Farey search for angle.
fareySearch :: FareyAngle -> FareyAngle -> InternalAngle -> FareyAngle
fareySearch l r t = let (a, b) = fareySearch' l r t in a <+> b

-- | Find a Farey angle for an internal angle.
fareyAngle :: InternalAngle -> FareyAngle
fareyAngle t | 0 < t && t < 1 = fareySearch fareyZero fareyOne t
-- deliberately unhandled

-- | Find the Farey parent angles for an internal angle.
fareyParents :: InternalAngle -> (FareyAngle, FareyAngle)
fareyParents t | 0 < t && t < 1 = fareySearch' fareyZero fareyOne t
-- deliberately unhandled

-- | Find a Farey angle for an internal angle p/q.
(</>) :: Integer -> Integer -> FareyAngle
p </> q = fareyAngle (InternalAngle (p % q))
infixl 7 </>
