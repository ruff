{-# LANGUAGE DeriveDataTypeable #-}
{- |
Module      :  Fractal.Mandelbrot.Symbolic.ConciseAddress
Copyright   :  (c) Claude Heiland-Allen 2010-2012
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  DeriveDataTypeable

Concise form of angled internal addresses.

-}
module Fractal.Mandelbrot.Symbolic.ConciseAddress
  ( ConciseAddress(ConciseAddress)
  , ConciseItem(ConcisePeriod, ConciseAngle)
  , toConciseAddress
  , fromConciseAddress
  , conciseItem
  ) where

import Data.Data (Data())
import Data.Typeable (Typeable())

import Fractal.Mandelbrot.Symbolic.ExternalAngle (Period())
import Fractal.Mandelbrot.Symbolic.InternalAngle (InternalAngle(), (*/))
import Fractal.Mandelbrot.Symbolic.AngledInternalAddress (AngledInternalAddress(Angled, Unangled))

-- | Concise angled internal address type.
newtype ConciseAddress = ConciseAddress [ConciseItem]
  deriving (Read, Show, Eq, Ord, Data, Typeable)

-- | Concise addresses are mixed sequences of periods and internal angles.
data ConciseItem = ConcisePeriod !Period | ConciseAngle !InternalAngle
  deriving (Read, Show, Eq, Ord, Data, Typeable)

-- | Deconstructor for items in the vein of 'either'.
conciseItem :: (Period -> a) -> (InternalAngle -> a) -> ConciseItem -> a
conciseItem f _ (ConcisePeriod p) = f p
conciseItem _ g (ConciseAngle r) = g r

-- | Reduce an angled internal address to concise form.
toConciseAddress :: AngledInternalAddress -> ConciseAddress
toConciseAddress = ConciseAddress . rule2 1 . rule1 . toItems
  where
    toItems (Unangled q) = ConcisePeriod q : []
    toItems (Angled q r a) = ConcisePeriod q : ConciseAngle r : toItems a
    rule1 = filter (/= ConciseAngle 0.5)
    rule2 _ [] = []
    rule2 p (ConcisePeriod q : rest)
      | p == q = rule2 q rest
      | otherwise = ConcisePeriod q : rule2 q rest
    rule2 p (ConciseAngle r : rest) = ConciseAngle r : rule2 (p */ r) rest

-- | Recreate an angled internal address from concise form.
fromConciseAddress :: ConciseAddress -> AngledInternalAddress
fromConciseAddress (ConciseAddress is0) = fromItems . rule1 . rule2 1 $ is0
  where
    fromItems [ConcisePeriod p] = Unangled p
    fromItems (ConcisePeriod p : ConciseAngle r : is) = Angled p r (fromItems is)
    rule2 p [] = [ConcisePeriod p]
    rule2 p (ConcisePeriod q : is)
      | p < q = ConcisePeriod p : rule2 q is
      | otherwise = rule2 q is
    rule2 p (ConciseAngle r : is) = ConcisePeriod p : ConciseAngle r : rule2 (p */ r) is
    rule1 (ConcisePeriod p : ConcisePeriod q : is) = ConcisePeriod p : ConciseAngle 0.5 : rule1 (ConcisePeriod q : is)
    rule1 (i : is) = i : rule1 is
    rule1 [] = []

{-
quickCheck (\k -> let r = flip approxRational 1e-3 . abs . snd . properFraction $ k
                  in 0 < r && r < 1 && odd (denominator r) ==>
                  (let ma = angledInternalAddress (ExternalAngle r)
                  in isJust ma ==>
                  ma == (fromConciseAddress . toConciseAddress) `fmap` ma))
-}
