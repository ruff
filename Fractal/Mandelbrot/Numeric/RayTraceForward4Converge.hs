{-# LANGUAGE DataKinds, FlexibleContexts, RankNTypes #-}
{- |
Module      :  Fractal.Mandelbrot.Numeric.RayTraceForward4Converge
Copyright   :  (c) Claude Heiland-Allen 2012,2014
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  DataKinds, FlexibleContexts, RankNTypes

Heuristics to detect convergence of 'RayTraceForward4'.

The general idea is as follows:

  * an island of size D is surrounded by an atom period domain of size
    proportional to sqrt D;

  * the core of the Newton's method basin of convergence is roughly the
    same as the period domain;

  * when all ray endpoints have atom period domain equal to the target
    period switch from tracing rays to Newton's method, using their
    midpoint as initial estimate.

Examples:

> convergeRays (ExternalAnglePair (11 / 75) (51264 / 349525)) (\c r -> show (c, r))

-}
module Fractal.Mandelbrot.Numeric.RayTraceForward4Converge
  ( convergeRays
  , raysConverged
  , addressToAtom
  ) where

{-
import Numeric.VariablePrecision
  ( NaturalNumber, precision, adjustPrecision, (.@$)
  , F, CF, magnitudeSquared, magnitude, scaleVComplex
  , N24
  )
-}

import Data.Complex
import Data.Proxy
import Numeric.Rounded
import Fractal.Mandelbrot.Utils

import Fractal.Mandelbrot.Symbolic.ExternalAngle
  ( ExternalAnglePair(ExternalAnglePair)
  , tunePair
  , anglePeriod
  , Period
  )
import Fractal.Mandelbrot.Numeric.RayTraceForward4
  ( RayTraceForward4Callback(RayTraceForward4Callback)
  , rayTraceForward4
  , rayTraceForwardStart4
  )
import Fractal.Mandelbrot.Symbolic.AngledInternalAddress
  ( AngledInternalAddress
  , externalAngles
  , addressPeriod
  )
import Fractal.Mandelbrot.Numeric.Atom
  ( Atom
  , convergeAtom
  )

type F p = Rounded TowardNearest p
type CF p = Complex (Rounded TowardNearest p)

-- | Given a ray pair converging at the root of a hyperbolic component,
--   find an estimate of its center and location using 'RayTraceForward4'.
convergeRays
  :: ExternalAnglePair {- ^ root angles -}
  -> (forall r . Precision r => CF r -> F r -> a) {- ^ callback -}
  -> a
convergeRays o@(ExternalAnglePair ol oh) f =
  let ExternalAnglePair il ih = tunePair h o
      h = ExternalAnglePair (1/3) (2/3)
      Just p = anglePeriod ol
      callback = RayTraceForward4Callback $ \continue zs -> case raysConverged p f zs of
        Nothing -> rayTraceForward4 continue callback
        Just j -> j
  in  rayTraceForward4 (rayTraceForwardStart4 (ol, oh, il, ih)) callback

-- | Lift convergence heuristics to multi-precision input.
raysConverged
  :: (Precision p, Precision q, Precision s, Precision t)
  => Period
  -> (forall r . Precision r => CF r -> F r -> a)
  -> (CF p, CF q, CF s, CF t)
  -> Maybe a
raysConverged p f = upConvert4 (raysConverged' p f)

-- | Convergence heuristics.
raysConverged'
  :: Precision r
  => Period
  -> (CF r -> F r -> a)
  -> (CF r, CF r, CF r, CF r)
  -> Maybe a
raysConverged' p f (a0, a1, b0, b1)
  | converged = Just (f c (magnitude (a - b)))
  | otherwise = Nothing
  where
    a = scaleComplex (-1) (a0 + a1)
    b = scaleComplex (-1) (b0 + b1)
    c = scaleComplex (-1) (a  + b )
    converged = all ((p ==) . atomPeriodDomain p) [a0,a1,b0,b1]

-- | Compute atom period domain for a point.
atomPeriodDomain
  :: Precision r
  => Period {- ^ maximum iterations -}
  -> CF r {- ^ point -}
  -> Period
atomPeriodDomain p c = go 0 0 (-1) 1e10
  where
    go q z mq md
      | q == p = mq
      | d < md = go q' z' q' d
      | otherwise = go q' z' mq md
      where
        q' = q + 1
        z' = z * z + c
        d = magnitudeSquared z'

-- | Increase precision of all to the maximum precision of all.
upConvert4
  :: (Precision p, Precision q, Precision s, Precision t)
  => (forall r . Precision r => (CF r, CF r, CF r, CF r) -> a)
  -> (CF p, CF q, CF s, CF t)-> a
upConvert4 f (a, b, c, d)
  | p >= m = f (a , b', c', d')
  | q >= m = f (a', b , c', d')
  | s >= m = f (a', b', c , d')
  | t >= m = f (a', b', c', d )
  where
    m = maximum [p, q, s, t]
    p = precision (realPart a)
    q = precision (realPart b)
    s = precision (realPart c)
    t = precision (realPart d)
    a', b', c', d' :: Precision r => CF r
    a' = recodeComplex a
    b' = recodeComplex b
    c' = recodeComplex c
    d' = recodeComplex d

-- | Find the atom for an angled internal address.
addressToAtom
  :: AngledInternalAddress {- ^ address -}
  -> (forall r . Precision r => Maybe (Atom r) -> a) {- ^ callback -}
  -> a
addressToAtom a f = -- FIXME split to parent island and walk through children
  let p = addressPeriod a
  in  case externalAngles a of
        Nothing -> reifyPrecision 0 (\p -> f (Nothing `asMayAtom` p))
        Just es -> convergeRays es $ \c _ ->
          (c .@$ (precision (realPart c) * 2)) (\c' ->
            convergeAtom p c' f)
  where
    asMayAtom :: Maybe (Atom q) -> Proxy q -> Maybe (Atom q)
    asMayAtom m _ = m

(.@$) :: forall p r . (Precision p) => CF p -> Int -> (forall q . (Precision q) => CF q -> r) -> r
(.@$) c p f = reifyPrecision p (\prox ->f (c `asComplexType` prox))
  where
    asComplexType :: (Precision s, Precision t) => CF s -> Proxy t -> CF t
    asComplexType x _ = recodeComplex x