{-# LANGUAGE BangPatterns, DataKinds, DeriveDataTypeable, PolyKinds, RankNTypes, ScopedTypeVariables, StandaloneDeriving #-}
{- |
Module      :  Fractal.Mandelbrot.Numeric.Atom
Copyright   :  (c) Claude Heiland-Allen 2011,2012,2014
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  BangPatterns, DataKinds, DeriveDataTypeable, PolyKinds, RankNTypes, ScopedTypeVariables, StandaloneDeriving

Mu-atom refinement.

-}
module Fractal.Mandelbrot.Numeric.Atom
  ( Atom(..)
  , AtomShape(Cardioid, Circular)
  , addressShape
  , canonicalizeAtom
  , convergeAtom
  , convergeNucleusBond
  , convergeNucleus
  , convergeBond
  , convergeInternal
  , analyseShape
  , atomDomainSize
  , atomScaling
  , atomScale
{-
  , DAtom
  , toDAtom
  , fromDAtom
  , withDAtom
-}
  ) where

import Data.Data (Data)
import Data.Typeable (Typeable)

import Data.List (inits)
import Data.Maybe (isNothing, fromJust)

{-
import Numeric.VariablePrecision
  ( VariablePrecision, NaturalNumber, Zero
  , F, adjustPrecision, (-@?), precision, (.@$)
  , CF, (.*), magnitudeSquared, sqr, phase, cis, scaleVComplex
  , toComplexDouble, fromComplexDouble, F24
  , DFloat, Complex, fromComplexDFloat, toComplexDFloat, withComplexDFloat
  )
-}

import Fractal.Mandelbrot.Symbolic.ExternalAngle (Period)
import Fractal.Mandelbrot.Symbolic.InternalAngle (InternalAngle)
import Fractal.Mandelbrot.Symbolic.AngledInternalAddress (AngledInternalAddress, splitAddress)

import Fractal.Mandelbrot.Utils
import Data.Proxy
import Data.Complex
import Numeric.Rounded

type F p = Rounded TowardNearest p
type CF p = Complex (Rounded TowardNearest p)

-- | An atom is a particular hyperbolic component, expressed in terms of
--   its concrete location in the complex plane.
data Atom (p :: *) =
  Atom
  { atomNucleus :: CF p
  , atomSize    :: Rounded TowardNearest 24
  , atomOrientation :: Double
  , atomPeriod  :: Period
  , atomShape :: AtomShape
  }
  deriving (Eq, Show, Typeable)
--deriving instance Precision p => Show (Atom p)

-- | The shape of an atom.
data AtomShape = Cardioid | Circular
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Data, Typeable)

-- | The shape of the atom corresponding to an address.
addressShape :: AngledInternalAddress {- ^ address -} -> AtomShape
addressShape addr = case splitAddress addr of
  (_, []) -> Cardioid
  _ -> Circular


(.@$) :: forall (p :: *) r . (Precision p) => CF p -> Int -> (forall (q :: *) . (Precision q) => CF q -> r) -> r
(.@$) c p f = reifyPrecision p (\prox ->f (c `asComplexType` prox))
  where
    asComplexType :: (Precision s, Precision t) => CF s -> Proxy t -> CF t
    asComplexType x _ = recodeComplex x

-- | Given an over-precise atom, canonicalize it to its sanest precision.
canonicalizeAtom :: forall (p :: *) a . Precision p => Atom p -> (forall r . (Precision r) => Atom r -> a) -> a
canonicalizeAtom a f = atomNucleus a .@$ p $ \n -> f a{ atomNucleus = n}
  where
    p = ceiling (24 - logBase 2 (atomSize a))

-- | Given the period and approximate nucleus location, refine this
--   estimate to an atom description, with incrementally increased
--   precision until the size is known to sufficiently useful effective
--   precision.
convergeAtom :: forall (p :: *) a . (Precision p) => Period {- ^ period -} -> CF p {- ^ nucleus estimate -} -> (forall (r :: *) . (Precision r) => Maybe (Atom r) -> a) -> a
convergeAtom period c f = convergeNucleusBond period c $ \mnb -> case mnb of
  Just (nucleus, bond) ->
    let delta = bond - nucleus
    in  case analyseShape period nucleus of
       Just shape -> canonicalizeAtom Atom
        { atomNucleus = nucleus
        , atomSize = sqrt (recodeFloat (magnitudeSquared delta))
        , atomOrientation = phase . recodeComplex $ delta
        , atomPeriod = period
        , atomShape = shape
        } (\x -> f (Just x))
       Nothing -> reifyPrecision 0 (\p -> f (Nothing `asMayAtom` p))
  Nothing -> reifyPrecision 0 (\p -> f (Nothing `asMayAtom` p))
  where
    asMayAtom :: Maybe (Atom q) -> Proxy q -> Maybe (Atom q)
    asMayAtom m _ = m

-- | Given the period and approximate nucleus location, successively
--   refine this estimate to the true nucleus and 1/2 bond point, with
--   incrementally increased precision until the difference between the
--   nucleus and bond is known to a sufficiently useful effective
--   precision.
convergeNucleusBond :: forall (p :: *) a . (Precision p) => Period {- ^ period -} -> CF p {- ^ nucleus estimate -} -> (forall (r :: *) . (Precision r) => Maybe (CF r, CF r) -> a) -> a
convergeNucleusBond p c f = do
  case (do
    n <- convergeNucleus p c
    b <- convergeBond p n (1/2)
    return (n, b)) of
    Nothing -> reifyPrecision 0 (\q -> f (Nothing `asMayPair` q))
    mnb@(Just (n, b)) ->
      if magnitudeSquared (n - b) > encodeFloat 1 (2 * (accuracy - fromIntegral (precision (realPart c))))
        then f mnb
        else n .@$ (precision (realPart n) * 2) $ \n' -> convergeNucleusBond p n' f
  where
    accuracy = 16 -- desired meaningful number of bits of delta
    asMayPair :: Maybe (CF q, CF q) -> Proxy q -> Maybe (CF q, CF q)
    asMayPair m _ = m


-- | Given the period and approximate location, successively refine
--   this estimate to a nucleus.
--
--   The algorithm is based on Robert Munafo's page
--   /Newton-Raphson method/
--   <http://mrob.com/pub/muency/newtonraphsonmethod.html>.
--
convergeNucleus :: Precision p => Period {- ^ period -} -> CF p {- ^ nucleus estimate -} -> Maybe (CF p)
convergeNucleus p c0 = go c0
  where
    accuracy = 4 -- converged when delta changed at most this many least significant bits
    go !c = step p 0 0
      where
        er = 65536
        er2 = er * er
        huge z = not (magnitudeSquared z < er2)
        step !q !z !d
          | huge z = Nothing
          | q == 0 = case c - z / d of
              c' | huge c' -> Nothing
                 | (c -@? c') < accuracy -> Just c'
                 | otherwise -> go c'
          | otherwise = step (q - 1) (sqr z + c) (scaleComplex 1 (z * d) + 1)

-- | Find a bond point to an atom at a particular internal angle.
convergeBond :: Precision p => Period {- ^ period -} -> CF p {- ^ nucleus -} -> InternalAngle {- ^ angle -} -> Maybe (CF p)
convergeBond p c a = convergeInternal p c 1 a

-- | Find an internal point within an atom.  The supplied radius should
--   be between 0 and 1.
convergeInternal :: Precision p => Period {- ^ period -} -> CF p {- ^ nucleus -} -> F p {- ^ radius -} -> InternalAngle {- ^ angle -} -> Maybe (CF p)
convergeInternal p c0 r0 a0 = go c0 c0
  where
    accuracy = 8 -- converged when delta changed at most this many least significant bits
    b0 = (r0:+0) * (recodeComplex . cis $ (2 * pi * realToFrac a0 :: Double))
    go !z1 !c1 = step p z1 1 0 0 0 c1 z1
    er = 65536
    er2 = er * er
    huge z = not (magnitudeSquared z < er2)
    next !dz !dc !dm !c1 !z1
      | dm == 0 = Nothing
      | huge z1 = Nothing
      | huge c1 = Nothing
      | accurate = Just c1'
      | otherwise = go z1' c1'
      where
        accurate = accurateZ && accurateC
        accurateZ = deltaZ < encodeFloat 1 (2 * (accuracy - floatDigits deltaZ))
        accurateC = deltaC < encodeFloat 1 (2 * (accuracy - floatDigits deltaC))
        deltaZ = magnitudeSquared (z1' - z1)
        deltaC = magnitudeSquared (c1' - c1)
        z1' = z1 + dz / dm
        c1' = c1 + dc / dm
    step !q !a !b !c !d !e !c1 !z1
      | q == 0 = next d0 d1 m c1 z1
      | otherwise = step (q - 1) (sqr a + c1) (scaleComplex 1 (a * b)) (scaleComplex 1 (sqr b + a * c)) (scaleComplex 1 (a * d) + 1) (scaleComplex 1 (a * e + b * d)) c1 z1
      where
        y0 = z1 - a
        y1 = b0 - b
        b1 = b - 1
        m = b1 * e - d * c
        d0 = y0 * e - d * y1
        d1 = b1 * y1 - y0 * c

-- | Given a period and nucleus, determine the shape of the corresponding
--   atom.  Might explode with division by zero if the precision of the
--   nucleus isn't sufficient.
analyseShape :: Precision p => Period {- ^ period -} -> CF p {- ^ nucleus -} -> Maybe AtomShape
analyseShape p n
  | failed = Nothing
  | ma > mi * threshold = Just Cardioid
  | otherwise = Just Circular
  where
    failed = isNothing mdeltas || mi == 0
    threshold = 2 * 2  -- empirical guess, 1 == perfect circle
    ma = maximum deltas
    mi = minimum deltas
    deltas = fromJust mdeltas
    mdeltas = sequence
      [ (magnitudeSquared . (n -)) `fmap` convergeBond p n a
      | a <- [1/17, 1/3, 1/2, 2/3, 16/17 ] -- somewhat arbitrary
      ]

-- | Given a period and nucleus, estimate the size of its atom domain.
--   <http://mathr.co.uk/blog/2013-12-10_atom_domain_size_estimation.html>
atomDomainSize :: Precision p => Period {- ^ period -} -> CF p {- ^ nucleus -} -> F p
atomDomainSize p c = go c 1 1 (magnitude c)
  where
    go z dc q mq
      | q == p    = mq / magnitude dc
      | mp < mq   = go z' dc' q' mp
      | otherwise = go z' dc' q' mq
      where
        z' = z * z + c
        dc' = 2 * z * dc + 1
        q' = q + 1
        mp = magnitude z

atomScaling :: Precision p => Period {- ^ period -} -> CF p {- ^ nucleus -} -> (CF p, CF p)
atomScaling p c = (beta, lambda)
  where
    lambda = product lambdas
    lambdas = drop 1 . take (fromIntegral p) . map (* 2) . iterate (\z -> z * z + c) $ 0
    beta = sum . map (recip . product) . inits $ lambdas

atomScale :: Precision p => Period {- ^ period -} -> CF p {- ^ nucleus -} -> CF p
atomScale p c = let (beta, lambda) = atomScaling p c in beta * lambda * lambda

{-
-- | Serializable form of Atom.
data DAtom =
  DAtom
  { datomNucleus :: Complex DFloat
  , datomSize    :: F24
  , datomOrientation :: Double
  , datomPeriod  :: Period
  , datomShape :: AtomShape
  }
  deriving (Eq, Read, Show, Typeable)

-- | Freeze an atom to serializable form.
toDAtom :: Precision p => Atom p -> DAtom
toDAtom a = DAtom
  { datomNucleus = toComplexDFloat (atomNucleus a)
  , datomSize = atomSize a
  , datomOrientation = atomOrientation a
  , datomPeriod = atomPeriod a
  , datomShape = atomShape a
  }

-- | Attempt to thaw an atom, returns Nothing on precision mismatch.
fromDAtom :: Precision p => DAtom -> Maybe (Atom p)
fromDAtom d = do
  n <- fromComplexDFloat (datomNucleus d)
  return Atom
    { atomNucleus = n
    , atomSize = datomSize d
    , atomOrientation = datomOrientation d
    , atomPeriod = datomPeriod d
    , atomShape = datomShape d
    }

-- | Thaw an atom to its natural precision.
withDAtom :: DAtom -> (forall p . Precision p => Maybe (Atom p) -> r) -> r
withDAtom d f = withComplexDFloat (datomNucleus d) $ f . fmap (\n -> Atom
    { atomNucleus = n
    , atomSize = datomSize d
    , atomOrientation = datomOrientation d
    , atomPeriod = datomPeriod d
    , atomShape = datomShape d
    })
-}