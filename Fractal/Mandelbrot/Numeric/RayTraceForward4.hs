{-# LANGUAGE DataKinds, FlexibleContexts, Rank2Types #-}
{- |
Module      :  Fractal.Mandelbrot.Numeric.RayTraceForward4
Copyright   :  (c) Claude Heiland-Allen 2012,2014
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  DataKinds, FlexibleContexts, Rank2Types

Tracing four external rays simultaneously (one pair landing at the root
and the other at the 1/2 bond point) can be used to locate a particular
hyperbolic component.

Example:

> main :: IO ()
> main = do
>   let callback = RayTraceForward4Callback $ \continue zs@(z1, z2, z3, z4) ->
>          show zs : rayTraceForward4 continue callback
>       ray angle = rayTraceForward4 (rayTraceForwardStart4 (ol, oh, il, ih)) callback
>         where Just o@(ExternalAnglePair ol oh) = externalAngles =<< angledInternalAddress angle
>               ExternalAnglePair il ih = tunePair h o
>               h = ExternalAnglePair (1/3) (2/3)
>   putStrLn . unlines . take 256 . ray $ 1/71

-}
module Fractal.Mandelbrot.Numeric.RayTraceForward4
  ( RayTraceForward4Callback(RayTraceForward4Callback)
  , RayTraceForward4(rayTraceForward4)
  , rayTraceForwardStart4
  ) where

--import Control.Parallel (par, pseq)
{-
import Numeric.VariablePrecision
  ( NaturalNumber
  , CF
  )
-}

import Fractal.Mandelbrot.Symbolic.ExternalAngle (ExternalAngle)
import Fractal.Mandelbrot.Numeric.RayTraceForward (RayTraceForwardCallback(RayTraceForwardCallback), RayTraceForward(rayTraceForward), rayTraceForwardStart)

import Data.Complex
import Numeric.Rounded

type CF p = Complex (Rounded TowardNearest p)

-- | For each four points on the rays, a callback gets passed the current
--   coordinates and aspect ratio, along with a continuation that can be
--   used to get more points.
data RayTraceForward4Callback a =
  RayTraceForward4Callback
  { rayTraceForward4Callback
      :: forall p q s t . (Precision p, Precision q, Precision s, Precision t)
      => RayTraceForward4 a
      -> (CF p, CF q, CF s, CF t)
      -> a
  }

-- | Step along the four rays by one point each, calling the callback.
data RayTraceForward4 a =
  RayTraceForward4
  { rayTraceForward4 :: RayTraceForward4Callback a -> a }

-- | Initialize ray tracing for an quadruple of external angles.
--
--   Precondition:
--     @(ol, oh, il, ih)@ forms two ray pairs, with @(ol, oh)@ landing
--     at the root cusp and @(il, ih)@ landing at the 1/2 bond point
--     of the same hyperbolic component.
rayTraceForwardStart4
  :: (ExternalAngle, ExternalAngle, ExternalAngle, ExternalAngle)
  -> RayTraceForward4 a
rayTraceForwardStart4 (ol, oh, il, ih) =
  RayTraceForward4
  { rayTraceForward4 = rayTraceForwardStep4 
      ( rayTraceForwardStart ol
      , rayTraceForwardStart oh
      , rayTraceForwardStart il
      , rayTraceForwardStart ih
      )
  }

rayTraceForwardStep4
  :: (RayTraceForward a, RayTraceForward a, RayTraceForward a, RayTraceForward a)
  -> RayTraceForward4Callback a
  -> a
rayTraceForwardStep4 (s4ol, s4oh, s4il, s4ih) callback =
  rayTraceForward s4ol $ RayTraceForwardCallback $ \col zol _ ->
  rayTraceForward s4oh $ RayTraceForwardCallback $ \coh zoh _ ->
  rayTraceForward s4il $ RayTraceForwardCallback $ \cil zil _ ->
  rayTraceForward s4ih $ RayTraceForwardCallback $ \cih zih _ ->
--  rayTraceForward4Callback callback (col `par` coh `par` cil `par` cih `par` zol `par` zoh `par` zil `par` zih `pseq` RayTraceForward4
  rayTraceForward4Callback callback RayTraceForward4
    { rayTraceForward4 = rayTraceForwardStep4 (col, coh, cil, cih) }
    (zol, zoh, zil, zih)
