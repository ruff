{-# LANGUAGE BangPatterns, DataKinds, FlexibleContexts, Rank2Types, TypeOperators #-}
{- |
Module      :  Fractal.Mandelbrot.Numeric.RayTraceForward
Copyright   :  (c) Claude Heiland-Allen 2011,2012,2014
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  BangPatterns, DataKinds, FlexibleContexts, Rank2Types, TypeOperators

Tracing external rays inwards from infinity with adaptive precision.

Example usage:

> main :: IO ()
> main = do
>   let callback = RayTraceForwardCallback $ \continue (x :+ y) _e ->
>          (show x ++ " " ++ show y) : rayTraceForward continue callback
>       ray angle = rayTraceForward (rayTraceForwardStart angle) callback
>   mapM_ (putStrLn . unlines . take 256 . ray) [0, 1/64 .. 63/64]

The algorithm is based on Tomoki Kawahira's paper
/An algorithm to draw external rays of the Mandelbrot set/
<http://www.math.nagoya-u.ac.jp/~kawahira/programs/mandel-exray.pdf>.

-}
module Fractal.Mandelbrot.Numeric.RayTraceForward
  ( RayTraceForwardCallback(RayTraceForwardCallback)
  , RayTraceForward(rayTraceForward)
  , rayTraceForwardStart
  ) where

{-
import Numeric.VariablePrecision
  ( NaturalNumber, SuccessorTo
  , VariablePrecision(adjustPrecision), (.@~)
  , F, CF, magnitudeSquared, mkPolar
  , fromComplexFloat, fromComplexDouble, sqr, scaleVComplex
  , cf8
  )
-}

import Data.Proxy
import GHC.TypeLits
import Data.Complex
import Numeric.Rounded
import Fractal.Mandelbrot.Utils
import Fractal.Mandelbrot.Symbolic.ExternalAngle (ExternalAngle, doubleAngle)


-- | For each point on the ray, a callback gets passed the current
--   coordinates and epsilon, along with a continuation that can be
--   used to get more points.
data RayTraceForwardCallback a = RayTraceForwardCallback{ rayTraceForwardCallback :: forall p . Precision p => RayTraceForward a -> Complex (Rounded TowardNearest p) -> Rounded TowardNearest p -> a }

-- | Step along the ray by one point, calling the callback.
data RayTraceForward a = RayTraceForward { rayTraceForward :: RayTraceForwardCallback a -> a }

-- | Initialize ray tracing for an external angle.
rayTraceForwardStart :: ExternalAngle -> RayTraceForward a
rayTraceForwardStart e = RayTraceForward{ rayTraceForward = rayTraceForwardStep (RayTraceForwardContext e 4 4 4 65536 1 (fromComplexDouble (mkPolar 65536 (realToFrac e)) :: Complex (Rounded TowardNearest Double)) 0 0) }

data RayTraceForwardContext p =
  RayTraceForwardContext
  { rtfcAngle :: !ExternalAngle
  , rtfcSharpness :: !Int -- number of steps to take within each dwell band
  , rtfcPrecision :: !Int -- enough bits must be available to represent the delta with this much effective precision
  , rtfcAccuracy  :: !Int -- scales epsilon relative to the length of the last step
  , rtfcEscapeR :: !Double
  , rtfcEpsilon2 :: !(Rounded TowardNearest p)
  , rtfcLastC :: !(Complex (Rounded TowardNearest p))
  , rtfcStepK :: !Int
  , rtfcStepJ :: !Int
  }

rayTraceForwardStep :: Precision q => RayTraceForwardContext q -> RayTraceForwardCallback a -> a
rayTraceForwardStep rtfc0 go = step rtfc0 go
  where
    step :: Precision q => RayTraceForwardContext q -> RayTraceForwardCallback a -> a
    step rtfc g
      | rtfcStepJ rtfc >= rtfcSharpness rtfc = step rtfc
          { rtfcAngle = doubleAngle (rtfcAngle rtfc)
          , rtfcStepK = rtfcStepK rtfc + 1
          , rtfcStepJ = 0
          } g
      | otherwise = newton (rtfcLastC rtfc) (rtfcEpsilon2 rtfc) 0 g
      where
        limit = 64 -- FIXME arbitrary Newton iteration count limit
        r0 = rtfcEscapeR rtfc ** ((1/2) ** (fromIntegral (rtfcStepJ rtfc + 1) / fromIntegral (rtfcSharpness rtfc)))
        t0 = fromComplexDouble $ mkPolar r0 (2 * pi * realToFrac (rtfcAngle rtfc))
        newton :: Precision p => Complex (Rounded TowardNearest p) -> Rounded TowardNearest p -> Int -> RayTraceForwardCallback a -> a
        newton !z !e2 !p f
          | enoughBits && converged =
              let lastC = recodeComplex (rtfcLastC rtfc)
                  eps' = scaleFloat (negate (2 * rtfcAccuracy rtfc)) (magnitudeSquared (z' - lastC))
              in  rayTraceForwardCallback f (RayTraceForward (rayTraceForwardStep rtfc
                    { rtfcStepJ = rtfcStepJ rtfc + 1
                    , rtfcLastC = z'
                    , rtfcEpsilon2 = eps'
                    })) z' eps'
          | enoughBits && p < limit = newton z' e2 (p + 1) f
          | otherwise = reifyPrecision (1 + precision (realPart z)) (\q ->
             newton (bumpPrecisionC z' q) (bumpPrecision e2 q) 0 f)
          where
            enoughBits = negate (exponent e2) < 2 * (floatDigits e2 - rtfcPrecision rtfc)
            converged  = delta < e2
            delta = magnitudeSquared (z' - z)
            d = (cc - recodeComplex t0) / dd
            z' = z - d
            (cc, dd) = ncnd (rtfcStepK rtfc + 1)
            ncnd 1 = (z, 1)
            ncnd i = let (!nc, !nd) = ncnd (i - 1) in (sqr nc + z, scaleComplex 1 (nc * nd) + 1)
  
--bumpPrecision :: (VariablePrecision t, NaturalNumber p) => t p -> t (SuccessorTo p)
bumpPrecision :: (Precision p, Precision q) => Rounded TowardNearest p -> Proxy q -> Rounded TowardNearest q -- + 1
bumpPrecision x _ = recodeFloat x
bumpPrecisionC :: (Precision p, Precision q) => Complex (Rounded TowardNearest p) -> Proxy q -> Complex (Rounded TowardNearest q) -- + 1
bumpPrecisionC x _ = recodeComplex x

