{-# LANGUAGE BangPatterns, DataKinds, Rank2Types #-}
{- |
Module      :  Fractal.Mandelbrot.Numeric.RayTraceReverse
Copyright   :  (c) Claude Heiland-Allen 2011,2012,2014
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  BangPatterns, DataKinds, Rank2Types

Exterior parameters near the boundary can be traced outwards to compute
external angles.

Example usage:

> main :: IO ()
> main = do
>   let callback bs = RayTraceReverseCallback $ \m -> case m of
>         Nothing -> []
>         Just (continue, m') -> case m' of
>           Nothing -> let r = fromBits bs % (bit (length bs) - 1) in
>             "DONE" : ("\t" ++ show r) : ("\t" ++ show (fromRational r)) : rayTraceReverse continue (callback bs)
>           Just (c, m'') -> case m'' of
>             Nothing    -> (" \t" ++ show c) : rayTraceReverse continue (callback        bs )
>             Just False -> ("0\t" ++ show c) : rayTraceReverse continue (callback (False:bs))
>             Just True  -> ("1\t" ++ show c) : rayTraceReverse continue (callback (True :bs))
>       ray c = rayTraceReverse (rayTraceReverseStart c) (callback [])
>   putStr . unlines . ray . fromComplex fromDouble . read . head =<< getArgs

-}

module Fractal.Mandelbrot.Numeric.RayTraceReverse
  ( RayTraceReverse(rayTraceReverse)
  , RayTraceReverseCallback(RayTraceReverseCallback)
  , rayTraceReverseStart
  ) where

import Fractal.Mandelbrot.Utils (sqr, magnitudeSquared, toComplexDouble, fromComplexDouble, recodeComplex, scaleComplex)

{-
import Numeric.VariablePrecision
  ( NaturalNumber, adjustPrecision, F
  , Complex, CF, realPart, magnitudeSquared, sqr
  , scaleVComplex, fromComplexDouble, toComplexDouble, phase, mkPolar, fromDouble
  )
-}
import Data.Complex
import Numeric.Rounded

-- | For each point on the ray, a callback gets passed the current
--   coordinate and a bool whenever a dwell boundary is crossed
--   along with a continuation that can be used to get more points
--   if the ray hasn't passed the escape radius.
data RayTraceReverseCallback a = RayTraceReverseCallback{ rayTraceReverseCallback :: forall p . Precision p => Maybe (RayTraceReverse a, Maybe (Complex (Rounded TowardNearest p), Maybe Bool)) -> a }

-- | Step along the ray by one point, calling the callback.
data RayTraceReverse  a = RayTraceReverse { rayTraceReverse  :: RayTraceReverseCallback a -> a }

-- | Initialize ray tracing for a point.
rayTraceReverseStart :: Precision p => Complex (Rounded TowardNearest p) -> RayTraceReverse a
rayTraceReverseStart c = RayTraceReverse{ rayTraceReverse = rayTraceReverseStep (RayTraceReverseContext 8 4 er eps2 c n z d) }
  where
    er = 1024
    er2 = er * er
    eps2 = encodeFloat 1 (2 * (4 - floatDigits (realPart c)))
    (n, z) = iter er2 c 0 0
    d = dwell er2 n z

data RayTraceReverseContext p =
  RayTraceReverseContext
  { rtrcSharpness :: !Int -- number of steps to take within each dwell band
  --  , rtrcPrecision :: !Int -- enough bits must be available to represent the delta with this much effective precision  FIXME implement incremental precision reduction
  , rtrcAccuracy  :: !Int -- scales epsilon relative to the length of the last step
  , rtrcEscapeR :: !Double
  , rtrcEpsilon2 :: !(Rounded TowardNearest p)
  , rtrcLastC :: !(Complex (Rounded TowardNearest p))
  , rtrcLastN :: !Integer
  , rtrcLastZ :: !(Complex Double)
  , rtrcLastD :: !Double
  }

iter :: Precision p => Double -> Complex (Rounded TowardNearest p) -> Integer -> Complex (Rounded TowardNearest p) -> (Integer, Complex Double)
iter er2 c = go
  where
    go !n !z
      | magnitudeSquared z' > er2 = (n, z')
      | otherwise = go (n + 1) (sqr z + c)
      where z' = toComplexDouble z

dwell :: Double -> Integer -> Complex Double -> Double
dwell er2 n z = fromIntegral n - logBase 2 (log (magnitudeSquared z) / log er2)

iterd :: Precision p => Complex (Rounded TowardNearest p) -> Integer -> Complex (Rounded TowardNearest p) -> Complex (Rounded TowardNearest p) -> (Complex (Rounded TowardNearest p), Complex (Rounded TowardNearest p))
iterd !c !m !z !dz
  | m == 0 = (z, dz)
  | otherwise = iterd c (m - 1) (sqr z + c) (scaleComplex 1 (z * dz) + 1)

rayTraceReverseStep :: Precision p => RayTraceReverseContext p -> RayTraceReverseCallback a -> a
rayTraceReverseStep rtrc go
  | escaped   = rayTraceReverseCallback go (Just (stop, Nothing `asTypeOf` Just (c, Nothing)))
  | otherwise = rayTraceReverseCallback go (Just (continue, Just (newC, newB)))
  where
    stop     = RayTraceReverse{ rayTraceReverse = \go' -> rayTraceReverseCallback go' (Nothing `asTypeOf` Just (undefined, Just (c, Nothing))) }
    continue = RayTraceReverse
      { rayTraceReverse = rayTraceReverseStep rtrc
        { rtrcEpsilon2 = scaleFloat (negate (2 * rtrcAccuracy rtrc)) (magnitudeSquared (newC - c))
        , rtrcLastC = newC
        , rtrcLastN = newN
        , rtrcLastZ = newZ
        , rtrcLastD = newD
        }
      }
    er2 = er * er
    er = rtrcEscapeR rtrc
    eps2 = rtrcEpsilon2 rtrc
    c = rtrcLastC rtrc
    n = rtrcLastN rtrc
    z = rtrcLastZ rtrc
    d = rtrcLastD rtrc
    d' = d - 1 / fromIntegral (rtrcSharpness rtrc)
    m = ceiling d'
    r = er ** (2 ** (fromIntegral m - d'))
    a = phase z / (2 * pi)
    t = a - fromIntegral (floor a :: Int)
    k0 = recodeComplex . fromComplexDouble $ mkPolar r (2 * pi *  t     )
    k1 = recodeComplex . fromComplexDouble $ mkPolar r (    pi *  t     )
    k2 = recodeComplex . fromComplexDouble $ mkPolar r (    pi * (t + 1))
    step !k !cc =
      let (f, df) = iterd cc m 0 0
          dc = (f - k) / df
          cc' = cc - dc
      in  cc : if not (magnitudeSquared (cc' - cc) > eps2) then [] else step k cc'
    steps k = step k c
    c0 = last (steps k0)
    (c1, c2) = last (steps k1 `zip` steps k2)
    dc1 = magnitudeSquared (c1 - c)
    dc2 = magnitudeSquared (c2 - c)
    (n0, z0) = iter er2 c0 0 0
    (n1, z1) = iter er2 c1 0 0
    (n2, z2) = iter er2 c2 0 0
    d0 = dwell er2 n0 z0
    d1 = dwell er2 n1 z1
    d2 = dwell er2 n2 z2
    (newC, newN, newZ, newD, newB)
      | m == n    = (c0, n0, z0, d0, Nothing)
      | dc1 < dc2 = (c1, n1, z1, d1, if n1 == n then Nothing else Just False)
      | dc2 < dc1 = (c2, n2, z2, d2, if n2 == n then Nothing else Just True )
      | otherwise = error $ show (dc1, dc2, m, n, n1, n2)
    escaped = m <= 0
