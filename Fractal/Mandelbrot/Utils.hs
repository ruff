{- |
Module      :  Fractal.Mandelbrot.Utils
Copyright   :  (c) Claude Heiland-Allen 2010-2012
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  portable

Miscellaneous utility functions.

-}
module Fractal.Mandelbrot.Utils
  ( fromBits
  , mod_
  , divisors
  , strictlyWithin
  , strictlyWithout
  , chunkWith2
  , genericElemIndex
  , magnitudeSquared
  , sqr
  , toComplexDouble
  , fromComplexDouble
  , recodeComplex
  , recodeFloat
  , scaleComplex
  , (-@?)
{-
  , safeGenericIndex
-}
  ) where

import Data.List (foldl')
import Data.Complex (Complex((:+)), magnitude)

magnitudeSquared :: Num r => Complex r -> r
magnitudeSquared (r:+i) = r*r + i*i

sqr :: Num r => Complex r -> Complex r
sqr (r:+i) = (r*r - i*i) :+ (2*r*i)

toComplexDouble :: RealFloat r => Complex r -> Complex Double
toComplexDouble = recodeComplex

fromComplexDouble :: RealFloat r => Complex Double -> Complex r
fromComplexDouble = recodeComplex

recodeComplex :: (RealFloat r, RealFloat t) => Complex r -> Complex t
recodeComplex (r:+i) = recodeFloat r :+ recodeFloat i

recodeFloat :: (RealFloat r, RealFloat t) => r -> t
recodeFloat = uncurry encodeFloat . decodeFloat

scaleComplex :: RealFloat r => Int -> Complex r -> Complex r
scaleComplex n (r:+i) = scaleFloat n r :+ scaleFloat n i

-- | Convert a big endian list of bits to an integer.
fromBits :: [Bool] -> Integer
fromBits = foldl' (\ a b -> 2 * a + if b then 1 else 0) 0

-- | Like @n `mod` d@ but with output in @[1..d]@.
mod_ :: (Eq a, Integral a) => a -> a -> a
n `mod_` d
  | m == 0 = d
  | otherwise = m
  where m = n `mod` d
infixl 7 `mod_`

-- | All divisors of a number.
divisors :: (Eq a, Integral a) => a -> [a]
divisors n = [ m | m <- [1 .. n], n `mod` m == 0 ]

-- | Inside the range?
strictlyWithin  :: Ord a => a -> (a, a) -> Bool
strictlyWithin  x (l, h) = l < x && x < h

-- | Outside the range?
strictlyWithout :: Ord a => a -> (a, a) -> Bool
strictlyWithout x (l, h) = x < l || h < x

-- | Split a list 2 elements at a time and combine.
chunkWith2 :: (a -> a -> b) -> [a] -> [b]
chunkWith2 _ [] = []
chunkWith2 f (x:y:zs) = f x y : chunkWith2 f zs
-- deliberately unhandled

-- | Generic version of 'elemIndex'.
genericElemIndex :: (Eq a, Integral b) => a -> [a] -> Maybe b
genericElemIndex _ [] = Nothing
genericElemIndex e (f:fs)
  | e == f = Just 0
  | otherwise = (1 +) `fmap` genericElemIndex e fs

-- | A measure of meaningful precision in the difference of two
--   finite non-zero values.
--
--   Values of very different magnitude have little meaningful
--   difference, because @a + b `approxEq` a@ when @|a| >> |b|@.
--
--   Very close values have little meaningful difference,
--   because @a + (a - b) `approxEq` a@ as @|a| >> |a - b|@.
--
--   'effectivePrecisionWith' attempts to quantify this.
--
effectivePrecisionWith :: (Num t, RealFloat r) => (t -> r) {- ^ norm -} -> t -> t -> Int
effectivePrecisionWith n i j
  | t a && t b && t c = p - (d `max` (e - d))
  | otherwise = 0
  where
    t k = k > 0 && not (isInfinite k)
    d = (x `max` y) - z
    e = abs (x - y) `min` p
    p = floatDigits a
    x = exponent a
    y = exponent b
    z = exponent c
    a = n i
    b = n j
    c = n (i - j)

-- | Much like 'effectivePrecisionWith' combined with 'normInfinity'.
effectivePrecision :: (Num t, RealFloat t) => Complex t -> Complex t -> Int
effectivePrecision = effectivePrecisionWith magnitude
infix 6 `effectivePrecision`


-- | An alias for 'effectivePrecision'.
(-@?) :: (Num t, RealFloat t) => Complex t -> Complex t -> Int
(-@?) = effectivePrecision
infix 6 -@?

{-
-- | Safe version of 'genericIndex'.
safeGenericIndex :: Integral b => [a] -> b -> Maybe a
safeGenericIndex [] _ = Nothing
safeGenericIndex (x:xs) i
  | i < 0 = Nothing
  | i > 0 = safeGenericIndex xs (i - 1)
  | otherwise = Just x
-}
