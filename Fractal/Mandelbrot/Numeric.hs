{- |
Module      :  Fractal.Mandelbrot.Numeric
Copyright   :  (c) Claude Heiland-Allen 2010-2012
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  portable

Numeric algorithms for the Mandelbrot set.

-}
module Fractal.Mandelbrot.Numeric
  ( module Fractal.Mandelbrot.Numeric.Atom
  , module Fractal.Mandelbrot.Numeric.RayTraceForward
  , module Fractal.Mandelbrot.Numeric.RayTraceForward4
  , module Fractal.Mandelbrot.Numeric.RayTraceForward4Converge
  , module Fractal.Mandelbrot.Numeric.RayTraceReverse
  ) where

import Fractal.Mandelbrot.Numeric.Atom
import Fractal.Mandelbrot.Numeric.RayTraceForward
import Fractal.Mandelbrot.Numeric.RayTraceForward4
import Fractal.Mandelbrot.Numeric.RayTraceForward4Converge
import Fractal.Mandelbrot.Numeric.RayTraceReverse
