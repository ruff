{- |
Module      :  Fractal.Mandelbrot
Copyright   :  (c) Claude Heiland-Allen 2010-2012
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  portable

Convenience module importing everything.

-}
module Fractal.Mandelbrot
  ( module Fractal.Mandelbrot.Symbolic
  , module Fractal.Mandelbrot.Numeric
  , module Fractal.Mandelbrot.Utils
  , module Numeric.Rounded
  ) where

import Fractal.Mandelbrot.Symbolic
import Fractal.Mandelbrot.Numeric
import Fractal.Mandelbrot.Utils
import Numeric.Rounded
