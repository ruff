module Main (main) where

import Fractal.Mandelbrot

import Control.Monad (forM_, when)
import Data.Ratio ((%), numerator, denominator)

isIsland :: AngledInternalAddress -> Bool
isIsland = null . snd . splitAddress

main :: IO ()
main = do
  putStrLn "# period,island,address,numLo,denLo,numHi,denHi,orient,size,real,imag"
  putStrLn "1,True,1,0,1,1,1,3.141592653589793,0.75,0.0,0.0"
  forM_ [1..] $ \p -> do
    let den = 2 ^ p - 1
    forM_ [1 .. (den + 1) `div` 2 - 1] $ \num -> do
      let r = ExternalAngle (num % den)
          Just q = anglePeriod r
          Just ad = angledInternalAddress r
          Just (ExternalAnglePair s@(ExternalAngle elo) (ExternalAngle ehi)) = externalAngles ad
      when (q == p && isIsland ad && r == s) $ do
        addressToAtom ad $ \ma -> case ma of
          Nothing ->
            putStrLn
              $  show (toInteger p) ++ ",True,"
              ++ prettyAddress ad ++ ","
              ++ show (numerator elo) ++ ","
              ++ show (denominator elo) ++ ","
              ++ show (numerator ehi) ++ ","
              ++ show (denominator ehi) ++ ",?,?,?,?"
          Just a  ->
            putStrLn
              $  show (toInteger p) ++ ",True,"
              ++ prettyAddress ad ++ ","
              ++ show (numerator elo) ++ ","
              ++ show (denominator elo) ++ ","
              ++ show (numerator ehi) ++ ","
              ++ show (denominator ehi) ++ ","
              ++ show (atomOrientation a) ++ ","
              ++ show (atomSize a) ++ ","
              ++ show (realPart (atomNucleus a)) ++ ","
              ++ show (if abs (imagPart (atomNucleus a)) < auto (atomSize a / 16) then 0 else imagPart (atomNucleus a))
