{-# LANGUAGE RankNTypes #-}
module Main (main) where

import Fractal.Mandelbrot
import Fractal.Mandelbrot.Numeric.RayTraceForward4ConvergeP (convergeRaysP)

import Control.Monad (forM_, replicateM_, when)
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import Data.Maybe (fromJust)
import Data.Ratio ((%))
import Data.Time.Clock (getCurrentTime, diffUTCTime, NominalDiffTime)
import System.IO (hPutStrLn, stderr, hFlush, stdout)
import System.Random (randomRIO)

modifyIORef'' :: IORef (Double, Double) -> ((Double, Double) -> (Double, Double)) -> IO ()
modifyIORef'' r f = do
  p <- readIORef r
  let q@(x, y) = f p
  x `seq` y `seq` writeIORef r q

time :: ((forall n . NaturalNumber n => CF n -> F n -> IO String) -> IO String) -> (forall n . NaturalNumber n => CF n -> IO String) -> IO String
time f g = do
  t0 <- getCurrentTime
  f (\c _ -> do
    t1 <- getCurrentTime
    r  <- g c
    t2 <- getCurrentTime
    return (show (s $ t1 `diffUTCTime` t0, s $ t2 `diffUTCTime` t1, s $ t2 `diffUTCTime` t0, r)))
  where
    s = realToFrac :: NominalDiffTime -> Double

eprint :: String -> IO ()
eprint = hPutStrLn stderr

main = do
  forM_ [2 ..] $ \p -> do
    stats <- newIORef (0, 0)
    let den = 2 ^ p - 1
    replicateM_ 10 $ do
      num <- randomRIO (1, den - 1)
      let r = ExternalAngle (num % den)
      when (anglePeriod r == Just p) $ do
        let Just ea = externalAngles =<< angledInternalAddress r
        eprint (show r)
        s  <- time (convergeRays  ea) (\c -> convergeAtom p c (\x -> return (show x)))
        eprint s
        sP <- time (convergeRaysP ea) (\c -> convergeAtom p c (\x -> return (show x)))
        eprint sP
        let (t10, t21, t20, s') = read s :: (Double, Double, Double, String)
            Just a = read s' :: Maybe (Atom N53)
            (t10P, t21P, t20P, s'P) = read sP :: (Double, Double, Double, String)
            Just aP = read s'P :: Maybe (Atom N53)
        if check a aP then modifyIORef'' stats (\(t, tP) -> (t + t20, tP + t20P)) else print a >> print aP
    (t, tP) <- readIORef stats
    putStrLn (show (fromIntegral p :: Integer) ++ " " ++ show (t / tP))
    hFlush stdout

check a0 a1
  =  atomShape a0 == atomShape a1
  && atomPeriod a0 == atomPeriod a1
  && 0.5 < s && s < 2
  && magnitude d < auto s / 16
  where
    s = atomSize a0 / atomSize a1
    d = atomNucleus a0 - atomNucleus a1
