{-# LANGUAGE RankNTypes #-}
module Main (main) where

import Fractal.Mandelbrot
import Fractal.Mandelbrot.Numeric.RayTraceForward4ConvergeP (convergeRaysP)

import Control.Monad (forM_, replicateM_, when)
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import Data.Maybe (fromJust)
import Data.Ratio ((%), numerator, denominator)
import Data.Time.Clock (getCurrentTime, diffUTCTime, NominalDiffTime)
import System.IO (hPutStrLn, stderr, hFlush, stdout)
import System.Random (randomRIO)

import Debug.Trace (traceShow)

modifyIORef'' :: IORef (Double, Double) -> ((Double, Double) -> (Double, Double)) -> IO ()
modifyIORef'' r f = do
  p <- readIORef r
  let q@(x, y) = f p
  x `seq` y `seq` writeIORef r q

time :: IO r -> IO (Double, r)
time f = do
  t0 <- getCurrentTime
  r <- f
  t1 <- getCurrentTime
  return (s $ t1 `diffUTCTime` t0, r)
  where
    s = realToFrac :: NominalDiffTime -> Double

eprint :: String -> IO ()
eprint = hPutStrLn stderr

isIsland = null . snd . splitAddress

prettyAngledInternalAddress (Angled (Period p) (InternalAngle r) a) = show p ++ (if r == 0.5 then " " else " " ++ show (numerator r) ++ "/" ++ show (denominator r) ++ " ") ++ prettyAngledInternalAddress a
prettyAngledInternalAddress (Unangled (Period p)) = show p

main = do
  forM_ (iterate (2 *) 128) $ \p -> do
    stats <- newIORef (0, 0)
    let den = 2 ^ p - 1
    replicateM_ 2 $ do
      num <- randomRIO (1, den - 1)
      let r = ExternalAngle (num % den)
          Just q = anglePeriod r
          Just ad = angledInternalAddress r
          Just ea = externalAngles ad
      when (q == p && isIsland ad) $ do
        putStrLn (prettyAngledInternalAddress ad)
        (tP, (dP, eP)) <- time (convergeRaysP ea $ \c e -> c .@$ (precision c * 2) $ \c' -> convergeAtom p c' (\(Just x) -> return (toDAtom x, toDFloat e)))
        withDAtom dP print
        (t,  (d , e )) <- time (convergeRays  ea $ \c e -> c .@$ (precision c * 2) $ \c' -> convergeAtom p c' (\(Just x) -> return (toDAtom x, toDFloat e)))
        withDAtom d print
        withDAtom d $ \(Just a) -> withDAtom dP $ \(Just aP) -> if check a aP
          then modifyIORef'' stats (\(t0, tP0) -> (t0 + t, tP0 + tP))
          else putStrLn "FAIL"
    (t, tP) <- readIORef stats
    putStrLn (show (fromIntegral p :: Integer) ++ " " ++ show (t / tP))
    hFlush stdout

check a0 a1
  =  atomShape a0 == atomShape a1
  && atomPeriod a0 == atomPeriod a1
  && 0.5 < s && s < 2
  && magnitude d < auto s / 16
  where
    s = atomSize a0 / atomSize a1
    d = atomNucleus a0 - auto (atomNucleus a1)
